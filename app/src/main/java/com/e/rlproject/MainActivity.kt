package com.e.rlproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.e.remotelogger.RemoteLogger
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        RemoteLogger.init(this, "release", packageName)
        login.setOnClickListener {
            RemoteLogger.login("+38080808080")
        }
        log.setOnClickListener {
            RemoteLogger.d("someTag", "some message")
        }
    }
}
