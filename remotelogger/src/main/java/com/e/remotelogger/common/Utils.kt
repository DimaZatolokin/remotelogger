package com.e.remotelogger.common

import android.os.Build
import android.content.pm.PackageManager
import android.content.Context


fun getDeviceInfo(): String {
    return "${Build.MANUFACTURER} ${Build.MODEL}, Android version ${Build.VERSION.SDK_INT}"
}

fun getAppVersionName(context: Context): String {
    try {
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        return pInfo.versionName
    } catch (e: PackageManager.NameNotFoundException) {
    }
    return ""
}

fun getAppVersionCode(context: Context): Int {
    try {
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        return pInfo.versionCode
    } catch (e: PackageManager.NameNotFoundException) {
    }
    return 0
}
