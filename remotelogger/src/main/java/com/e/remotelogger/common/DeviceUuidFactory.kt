package com.e.remotelogger.common

import android.content.Context
import android.content.pm.PackageManager
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat
import java.io.UnsupportedEncodingException
import java.util.*

private const val PREFS_FILE = "device_id.xml"
private const val PREFS_DEVICE_ID = "device_id"

internal class DeviceUuidFactory private constructor() {
    companion object {
        private var uuid: UUID? = null

        fun init(context: Context) {
            if (uuid == null) {
                synchronized(DeviceUuidFactory::class) {
                    val prefs = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
                    val id = prefs.getString(PREFS_DEVICE_ID, null)

                    id?.let {
                        uuid = UUID.fromString(it)
                    } ?: run {
                        val androidId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
                        // Use the Android ID unless it's broken, in which case fallback on deviceId,
                        // unless it's not available, then fallback on a random number which we store
                        // to a prefs file
                        uuid = try {
                            if ("9774d56d682e549c" == androidId) {
                                val deviceId = if (ContextCompat.checkSelfPermission(
                                        context,
                                        android.Manifest.permission.READ_PHONE_STATE
                                    )
                                    == PackageManager.PERMISSION_GRANTED
                                ) {
                                    (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).deviceId
                                } else null
                                deviceId?.let {
                                    UUID.nameUUIDFromBytes(it.toByteArray(charset("utf8")))
                                } ?: UUID.randomUUID()
                            } else {
                                UUID.nameUUIDFromBytes(androidId.toByteArray(charset("utf8")))
                            }
                        } catch (e: UnsupportedEncodingException) {
                            throw RuntimeException(e)
                        }
                        // Write the value out to the prefs file
                        prefs.edit().putString(PREFS_DEVICE_ID, uuid.toString()).apply()
                    }
                }
            }
        }

        fun getDeviceUuid(): String? {
            return uuid.toString()
        }
    }
}