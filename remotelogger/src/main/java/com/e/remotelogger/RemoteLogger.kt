package com.e.remotelogger

import android.content.Context
import android.util.Log
import com.e.remotelogger.data.Repository

private const val TYPE_INFO = "info"
private const val TYPE_DEBUG = "debug"
private const val TYPE_VERBOSE = "verbose"
private const val TYPE_WARN = "warn"
private const val TYPE_ERROR = "error"

private const val BUILD_TYPE_RELEASE = "release"

class RemoteLogger {

    companion object {

        private var repository: Repository? = null

        fun init(
            context: Context,
            buildType: String,
            applicationId: String
        ) {
            if (buildType == BUILD_TYPE_RELEASE) {
                Repository.init(context, applicationId)
                repository = Repository.getInstance()
            }
        }

        fun login(userName: String) {
            repository?.loginEvent(userName)
        }

        private fun log(type: String, tag: String, message: String) {
            when (type) {
                TYPE_ERROR -> {
                    Log.e(tag, message)
                    repository?.logEvent(tag, message, 5)
                }
                TYPE_WARN -> {
                    Log.w(tag, message)
                    repository?.logEvent(tag, message, 4)
                }
                TYPE_INFO -> {
                    Log.i(tag, message)
                    repository?.logEvent(tag, message, 3)
                }
                TYPE_DEBUG -> {
                    Log.d(tag, message)
                    repository?.logEvent(tag, message, 2)
                }
                else -> {
                    Log.v(tag, message)
                    repository?.logEvent(tag, message, 1)
                }
            }
        }

        fun i(tag: String, message: String) {
            log(TYPE_INFO, tag, message)
        }

        fun d(tag: String, message: String) {
            log(TYPE_DEBUG, tag, message)
        }

        fun e(tag: String, message: String) {
            log(TYPE_ERROR, tag, message)
        }

        fun w(tag: String, message: String) {
            log(TYPE_WARN, tag, message)
        }

        fun v(tag: String, message: String) {
            log(TYPE_VERBOSE, tag, message)
        }
    }
}
