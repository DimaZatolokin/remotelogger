package com.e.remotelogger.data.models.responses

internal class GetTokenResponse(val token: String)