package com.e.remotelogger.data.storage

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.e.remotelogger.data.models.LogEvent
import com.e.remotelogger.data.models.LoginEvent
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

private const val KEY_TOKEN = "KEY_TOKEN"
private const val KEY_LOGIN_EVENTS = "KEY_LOGIN_EVENTS"
private const val KEY_LOG_EVENTS = "KEY_LOG_EVENTS"

internal class Preferences private constructor(context: Context) {

    private val preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(
            "${context.packageName}.REMOTE_LOGGER_PREFERENCES_STORAGE",
            Context.MODE_PRIVATE
        )
    }

    fun setToken(token: String) {
        preferences.edit().putString(KEY_TOKEN, token).commit()
    }

    fun getToken() = preferences.getString(KEY_TOKEN, null)

    fun saveEvent(event: LoginEvent) {
        val loginEvents: HashSet<LoginEvent> = getLoginEvents().toHashSet()
        loginEvents.add(event)
        preferences.edit().putString(KEY_LOGIN_EVENTS, Gson().toJson(loginEvents.toList())).commit()
    }

    fun saveEvent(event: LogEvent) {
        val logEvents: HashSet<LogEvent> = getLogEvents().toHashSet()
        logEvents.add(event)
        preferences.edit().putString(KEY_LOG_EVENTS, Gson().toJson(logEvents.toList())).commit()
    }

    fun getLoginEvents(): List<LoginEvent> {
        preferences.getString(KEY_LOGIN_EVENTS, null)?.run {
            return Gson().fromJson(this, object : TypeToken<List<LoginEvent>>() {}.type)
        } ?: return listOf()
    }

    fun getLogEvents(): List<LogEvent> {
        preferences.getString(KEY_LOG_EVENTS, null)?.run {
            return Gson().fromJson(this, object : TypeToken<List<LogEvent>>() {}.type)
        } ?: return listOf()
    }

    fun deleteLoginEvents(loginEvents: List<LoginEvent>) {
        val events = getLoginEvents().toMutableList()
        loginEvents.forEach {
            events.remove(it)
            Log.d("RemoteLogger", "deleteLoginEvent() ${it.time}")
        }
        preferences.edit().putString(KEY_LOGIN_EVENTS, Gson().toJson(listOf<LoginEvent>())).commit()
        events.forEach { saveEvent(it) }
    }

    fun deleteLogEvents(logEvents: List<LogEvent>) {
        val events = getLogEvents().toMutableList()
        logEvents.forEach {
            events.remove(it)
            Log.d("RemoteLogger", "deleteLogEvent() ${it.time} ${it.log}")
        }
        preferences.edit().putString(KEY_LOG_EVENTS, Gson().toJson(listOf<LogEvent>())).commit()
        events.forEach { saveEvent(it) }
    }


    companion object {

        @Volatile
        private var INSTANCE: Preferences? = null

        fun getInstance(context: Context): Preferences {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: Preferences(context).also { INSTANCE = it }
            }
        }
    }
}
