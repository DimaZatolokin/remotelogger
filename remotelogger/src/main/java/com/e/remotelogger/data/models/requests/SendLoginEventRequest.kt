package com.e.remotelogger.data.models.requests

import com.e.remotelogger.data.models.LoginEvent

internal class SendLoginEventRequest(
    val token: String,
    val events: List<LoginEvent>
)