package com.e.remotelogger.data.models.requests

import com.google.gson.annotations.SerializedName

internal class GetTokenRequest(
    val uid: String,
    @SerializedName("bundle_id")
    val bundleId: String,
    val info: String
)