package com.e.remotelogger.data.network

internal sealed class ResultObject<out T : Any?>(val result: T?) {

    class SuccessResult<T : Any?>(val successResult: T) : ResultObject<T>(successResult)

    open class ErrorResult(val error: Throwable) : ResultObject<Nothing>(null)
}

internal class ConnectionError : ResultObject.ErrorResult(Throwable("Internet connection failed!"))

internal class AuthorizationError : ResultObject.ErrorResult(Throwable("Unauthorized! Access Denied!"))

internal suspend fun <T : Any?> ResultObject<T>.applyToSuccess(
    function: suspend (T) -> Unit
): ResultObject<T> = this.apply {
    when (this) {
        is ResultObject.SuccessResult -> {
            result?.let {
                function(it)
            }
        }
    }
}

internal suspend fun <T : Any?> ResultObject<T>.applyToConnectionError(
    function: suspend () -> Unit
): ResultObject<T> = this.apply {
    when (this) {
        is ConnectionError -> {
            function()
        }
    }
}

internal suspend fun <T : Any?> ResultObject<T>.applyToAuthorizationError(
    function: suspend () -> Unit
): ResultObject<T> = this.apply {
    when (this) {
        is AuthorizationError -> {
            function()
        }
    }
}
