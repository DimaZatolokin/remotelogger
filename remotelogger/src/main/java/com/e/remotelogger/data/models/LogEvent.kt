package com.e.remotelogger.data.models

internal data class LogEvent(
    val tag: String,
    val level: String,
    val log: String,
    val version_app: String,
    val time: Long
)