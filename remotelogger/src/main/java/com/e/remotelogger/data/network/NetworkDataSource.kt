package com.e.remotelogger.data.network

import android.net.ConnectivityManager
import com.e.remotelogger.data.models.requests.GetTokenRequest
import com.e.remotelogger.data.models.requests.SendLogEventRequest
import com.e.remotelogger.data.models.requests.SendLoginEventRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import java.io.IOException

private const val ERROR_UNAUTHORIZED = 401

internal class NetworkDataSource private constructor(
    private val apiService: ApiService,
    private val connectivityManager: ConnectivityManager
) {

    companion object {
        @Volatile
        private var INSTANCE: NetworkDataSource? = null

        fun getInstance(apiService: ApiService, connectivityManager: ConnectivityManager): NetworkDataSource =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: NetworkDataSource(apiService, connectivityManager).also {
                    INSTANCE = it
                }
            }
    }

    suspend fun getToken(request: GetTokenRequest) = apiService.getToken(request).runRequest()

    suspend fun sendLoginEvent(request: SendLoginEventRequest) = apiService.sendLoginEvent(request).runRequest()

    suspend fun sendLogEvent(request: SendLogEventRequest) = apiService.sendLogEvent(request).runRequest()


    private suspend fun <T : Any?> Call<T>.runRequest(): ResultObject<T> = withContext(Dispatchers.IO) {
        try {
            if (!isConnected()) {
                return@withContext ConnectionError()
            }
            val response = execute()
            if (response.isSuccessful) {
                ResultObject.SuccessResult(response.body())
            } else {
                if (response.code() == ERROR_UNAUTHORIZED) {
                    AuthorizationError()
                } else {
                    ResultObject.ErrorResult(Throwable(response.message()))
                }
            }
        } catch (e: IOException) {
            ResultObject.ErrorResult(e)
        }
    } as ResultObject<T>

    private fun isConnected(): Boolean {
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}
