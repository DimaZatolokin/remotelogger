package com.e.remotelogger.data.models.requests

import com.e.remotelogger.data.models.LogEvent

internal class SendLogEventRequest(
    val token: String,
    val events: List<LogEvent>
)