package com.e.remotelogger.data.network

import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit

private const val BASE_URL = "http://devcpp.ho.ua/"

internal class NetworkService private constructor() {

    private val retrofit: Retrofit

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun createService() = retrofit.create(ApiService::class.java)

    companion object {

        private var INSTANCE: NetworkService? = null

        val instance: NetworkService
            get() {
                if (INSTANCE == null) {
                    INSTANCE = NetworkService()
                }
                return INSTANCE as NetworkService
            }
    }
}
