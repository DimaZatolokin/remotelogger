package com.e.remotelogger.data.network

import com.e.remotelogger.data.models.requests.GetTokenRequest
import com.e.remotelogger.data.models.requests.SendLoginEventRequest
import com.e.remotelogger.data.models.requests.SendLogEventRequest
import com.e.remotelogger.data.models.responses.GetTokenResponse
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

internal interface ApiService {

    @POST("api/get_token.php")
    fun getToken(@Body request: GetTokenRequest): Call<GetTokenResponse>

    @POST("api/login_events.php")
    fun sendLoginEvent(@Body request: SendLoginEventRequest): Call<Unit>

    @POST("api/log_events.php")
    fun sendLogEvent(@Body request: SendLogEventRequest): Call<Unit>
}