package com.e.remotelogger.data

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import com.e.remotelogger.common.DeviceUuidFactory
import com.e.remotelogger.common.getAppVersionCode
import com.e.remotelogger.common.getAppVersionName
import com.e.remotelogger.common.getDeviceInfo
import com.e.remotelogger.data.models.LogEvent
import com.e.remotelogger.data.models.LoginEvent
import com.e.remotelogger.data.models.requests.GetTokenRequest
import com.e.remotelogger.data.models.requests.SendLogEventRequest
import com.e.remotelogger.data.models.requests.SendLoginEventRequest
import com.e.remotelogger.data.models.responses.GetTokenResponse
import com.e.remotelogger.data.network.*
import com.e.remotelogger.data.network.NetworkDataSource
import com.e.remotelogger.data.network.NetworkService
import com.e.remotelogger.data.network.ResultObject
import com.e.remotelogger.data.network.applyToSuccess
import com.e.remotelogger.data.storage.Preferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


internal class Repository private constructor(
    context: Context,
    private val bundleId: String
) : CoroutineScope {

    companion object {

        @Volatile
        private var INSTANCE: Repository? = null

        fun init(context: Context, bundleId: String) {
            if (INSTANCE == null) {
                INSTANCE = Repository(context, bundleId)
            }
        }

        fun getInstance(): Repository = synchronized(this) {
            INSTANCE?.let {
                return INSTANCE as Repository
            } ?: throw RuntimeException("need to init Repository")
        }
    }

    override val coroutineContext = Dispatchers.Default

    private val appVersionCode: Int
    private val appVersionName: String
    private val connectivityManager: ConnectivityManager
    private val networkDataSource: NetworkDataSource
    private val preferences: Preferences

    init {
        val apiService = NetworkService.instance.createService()
        DeviceUuidFactory.init(context)
        appVersionCode = getAppVersionCode(context)
        appVersionName = getAppVersionName(context)
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkDataSource = NetworkDataSource.getInstance(apiService, connectivityManager)
        preferences = Preferences.getInstance(context)
    }

    private suspend fun obtainToken(): String? {
        val token = preferences.getToken()
        return if (token == null) {
            var result: String? = null
            refreshToken().applyToSuccess {
                result = it.token
            }
            result
        } else {
            Log.d("RemoteLogger", "obtainToken() get $token")
            token
        }
    }

    private suspend fun refreshToken(): ResultObject<GetTokenResponse> {
        DeviceUuidFactory.getDeviceUuid()?.run {
            return networkDataSource.getToken(GetTokenRequest(this, bundleId, getDeviceInfo())).applyToSuccess {
                preferences.setToken(it.token)
                Log.d("RemoteLogger", "refreshToken() success ${it.token}")
            }
        }
        Log.d("RemoteLogger", "refreshToken() no uid")
        return ResultObject.ErrorResult(Throwable())
    }

    fun loginEvent(userName: String) {
        launch {
            checkSavedEvents()
            val event = LoginEvent(userName, appVersionName, System.currentTimeMillis())
            sendLoginEvents(listOf(event))
                .applyToSuccess {
                    Log.d("RemoteLogger", "loginEvent() success")
                }.applyToConnectionError {
                    Log.d("RemoteLogger", "loginEvent() connection error ${event.time}")
                    preferences.saveEvent(event)
                }.applyToAuthorizationError {
                    refreshToken().applyToSuccess {
                        loginEvent(userName)
                    }
                }
        }
    }

    fun logEvent(tag: String, log: String, level: Int) {
        launch {
            checkSavedEvents()
            val event = LogEvent(
                tag,
                level.toString(),
                log,
                appVersionName,
                System.currentTimeMillis()
            )
            sendLogEvents(listOf(event))
                .applyToSuccess {
                    Log.d("RemoteLogger", "logEvent() success")
                }.applyToConnectionError {
                    Log.d("RemoteLogger", "logEvent() connection error ${event.time} ${event.log}")
                    preferences.saveEvent(event)
                }.applyToAuthorizationError {
                    refreshToken().applyToSuccess {
                        logEvent(tag, log, level)
                    }
                }
        }
    }

    private suspend fun sendLoginEvents(events: List<LoginEvent>): ResultObject<Unit> {
        val token = obtainToken()
        token?.run {
            return networkDataSource.sendLoginEvent(
                SendLoginEventRequest(this, events)
            )
        }
        return ResultObject.ErrorResult(Throwable())
    }

    private suspend fun sendLogEvents(events: List<LogEvent>): ResultObject<Unit> {
        val token = obtainToken()
        token?.run {
            return networkDataSource.sendLogEvent(SendLogEventRequest(token, events))
        }
        return ResultObject.ErrorResult(Throwable())
    }

    private fun checkSavedEvents() {
        launch {
            val loginEvents = preferences.getLoginEvents()
            if (loginEvents.isNotEmpty()) {
                val sendLoginEventsResult = sendLoginEvents(loginEvents)
                if (sendLoginEventsResult is ResultObject.SuccessResult) {
                    preferences.deleteLoginEvents(loginEvents)
                }
            }
            val logEvents = preferences.getLogEvents()
            if (logEvents.isNotEmpty()) {
                val sendLogEventsResult = sendLogEvents(logEvents)
                if (sendLogEventsResult is ResultObject.SuccessResult) {
                    preferences.deleteLogEvents(logEvents)
                }
            }
        }
    }
}
