package com.e.remotelogger.data.models

import com.google.gson.annotations.SerializedName

internal data class LoginEvent(
    @SerializedName("user_name")
    val userName: String,
    @SerializedName("version_app")
    val versionApp: String,
    val time: Long
)